function posCord(pos) {
  const letras = { 'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9 };
  if (pos.length === 2) {
    return { x: letras[pos[0]], y: Number(pos[1]) - 1 }
  } else if (pos.length === 3) {
    return { x: letras[pos[0]], y: 9 }
  }
}

function usuarioGano() {
  $('#botones-juego').hide();
  $('#info').hide();
  $('#log').hide();
  $('#reiniciar').show();
  $('#gui').prepend('<p>GANASTE!!!</p>');
}

function usuarioPerdio() {
  $('#botones-juego').hide();
  $('#info').hide();
  $('#log').hide();
  $('#reiniciar').show();
  $('#gui').prepend('<p>Perdiste :(</p>');
}

function CordPos(cord) {
  const letras = { '0': 'A', '1': 'B', '2': 'C', '3': 'D', '4': 'E', '5': 'F', '6': 'G', '7': 'H', '8': 'I', '9': 'J' };
  let letra = letras[cord['x'].toString()];
  let numero = cord['y'] + 1;
  numero = numero.toString();
  return letra + numero;
}

function resetearTablero() {
  $('.cuadro').css("background-color", "white");
  $('[id$=-casilla]').off('click');
}

function cambiarBoteEnTablero(barcoPos, nuevaPos, barco) {
  if (CordPos(barcoPos).length === 2) {
    idBarcoActual = '#' + CordPos(barcoPos)[0] + '-' + CordPos(barcoPos)[1] + '-' + 'casilla';
    $(idBarcoActual).html('');
    idBarcoNuevaPos = '#' + CordPos(nuevaPos)[0] + '-' + CordPos(nuevaPos)[1] + '-' + 'casilla';
    $(idBarcoNuevaPos).html(barco);
  } else if (CordPos(barcoPos).length === 3) {
    idBarcoActual = '#' + CordPos(barcoPos)[0] + '-' + '10' + '-' + 'casilla';
    $(idBarcoActual).html('');
    idBarcoNuevaPos = '#' + CordPos(nuevaPos)[0] + '-' + '10' + '-' + 'casilla';
    $(idBarcoNuevaPos).html(barco);
  }

}

function parsearAction(body) {
  let action = body['action'];
  if (action['type'] == 'FIRE') {
    let texto = 'FIRE' + ' - ' + action['ship'] + ' - ' + CordPos({ 'x': action['column'], 'y': action['row'] });
    return texto;
  } else if (action['type'] == 'MOVE') {
    let texto = 'MOVE' + ' - ' + action['ship'] + ' - ' + action['direction'] + ' - ' + action['quantity'];
    return texto;
  }
}

function parsearEvento(body) {
  let evento = body['events'];
}

function imprimirLog(usuario, texto) {
  let informacion = $('<p></p>').addClass('info-log').text(`${usuario}: ${texto}`);
  $('#log').prepend(informacion);
}

function anotarMovidaUsuario(barco, direction, quantity) {
  let texto = 'MOVE' + ' - ' + barco + ' - ' + direction + ' - ' + quantity;
  imprimirLog('[USUARIO]', texto);
}

function anotarDisparoUsuario(barco, direccion) {
  let texto = 'FIRE' + ' - ' + barco + ' - ' + CordPos(direccion);
  imprimirLog('[USUARIO]', texto);
}


function realizarAcciones(respuesta) {
  console.log(`respuesta: ${respuesta}`);
  let action = respuesta['action'];
  if (action['type'] == 'FIRE') {
    let column = action['column'];
    let row = action['row'] + 1;
    const letras = { '0': 'A', '1': 'B', '2': 'C', '3': 'D', '4': 'E', '5': 'F', '6': 'G', '7': 'H', '8': 'I', '9': 'J' };
    let idCasilla = '#' + letras[column.toString()] + '-' + row.toString() + '-' + 'casilla';
    let contenidoCasilla = $(idCasilla).html();
    console.log('id casilla:');
    console.log(idCasilla);
    if (contenidoCasilla && contenidoCasilla !== 'RIP') {
      imprimirLog('[Usuario]', '[Destroyed] Ship ' + contenidoCasilla);
      $(idCasilla).html('RIP').addClass('RIP');
      cantidadRips = $('.RIP').length;
      if (cantidadRips >= 10) {
        usuarioPerdio();
      }
    }
  }
}

async function postAction(token, gameId, body) {
  console.log(body);
  let respuesta = await fetch('https://battleship.iic2513.phobos.cl/games/' + gameId + '/action', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify(body),
  });

  respuesta = await respuesta.json();
  return respuesta
}

async function realizarEventos(respuesta) {
  try {
    eventos = respuesta['events'];
    eventos.forEach(element => {
      if (element['type'] === 'HIT_SHIP') {
        imprimirLog('[COMPUTADOR]', '[HIT] Ship ' + element['ship'])
      }
      if (element['type'] === 'SHIP_DESTROYED') {
        imprimirLog('[COMPUTADOR]', '[DESTROYED] Ship ' + element['ship'])
      }
      if (element['type'] === 'ALL_SHIPS_DESTROYED') {
        imprimirLog('[COMPUTADOR]', '[DESTROYED] Ship ' + element['ship'])
        usuarioGano();
      }
    });
  } catch (error) {
    console.log(error);
  }
}

async function postMove(token, gameId, ship, direction, quantity) {
  let body = {
    "action": {
      "type": "MOVE",
      "ship": ship,
      "direction": direction,
      "quantity": quantity
    }
  }
  respuesta = await postAction(token, gameId, body);
  realizarAcciones(respuesta);
  let texto = parsearAction(respuesta);
  imprimirLog('[COMPUTADOR]', texto);
  realizarEventos(respuesta);
  console.log(respuesta);
}

async function postShot(token, gameId, barco, cordDisparo) {
  let body = {
    "action": {
      "type": "FIRE",
      "ship": barco,
      "column": cordDisparo['x'],
      "row": cordDisparo['y'],
    }
  }
  respuesta = await postAction(token, gameId, body);
  realizarAcciones(respuesta);
  let texto = parsearAction(respuesta);
  imprimirLog('[COMPUTADOR]', texto);
  realizarEventos(respuesta);
  console.log(respuesta);
}

function moverBote(token, gameId) {
  $('[id$=-casilla]').click(function () {
    let barco = $(this).text();
    let casilla = this.id.split('-')[0] + this.id.split('-')[1];
    console.log(casilla);
    // if (barco !== '') {
    if (barco && barco !== 'RIP') {
      $(this).css("background-color", "green");
      escribirInfo(`Elige donde mover ${barco}`);
      $('[id$=-casilla]').click(function () {
        let letra = this.id.split('-')[0];
        let numero = this.id.split('-')[1];
        let barcoEnNuevaPos = $(this).html();
        let barcoPos = posCord(casilla);
        let nuevaPos = posCord(letra + numero);
        if (barcoEnNuevaPos !== '') {
          resetearTablero();
          iniciarTurno();
        } else {
          if (barcoPos === nuevaPos) {
            resetearTablero();
            iniciarTurno();
          } else if (barcoPos['x'] !== nuevaPos['x'] && barcoPos['y'] !== nuevaPos['y']) {
            resetearTablero();
            iniciarTurno();
          } else {
            const direccion = {};
            if (nuevaPos['x'] > barcoPos['x']) {
              // East
              direccion['dir'] = 'EAST';
              direccion['cant'] = nuevaPos['x'] - barcoPos['x'];
            } else if (nuevaPos['x'] < barcoPos['x']) {
              // West
              direccion['dir'] = 'WEST';
              direccion['cant'] = barcoPos['x'] - nuevaPos['x'];
            } else if (nuevaPos['y'] > barcoPos['y']) {
              // North
              direccion['dir'] = 'NORTH';
              direccion['cant'] = nuevaPos['x'] - barcoPos['x'];
            } else if (nuevaPos['y'] < barcoPos['y']) {
              // South
              direccion['dir'] = 'SOUTH';
              direccion['cant'] = barcoPos['y'] - nuevaPos['y'];
            }
            if (barco[0] === 'F') {
              if (direccion['cant'] <= 4) {
                cambiarBoteEnTablero(barcoPos, nuevaPos, barco);
                anotarMovidaUsuario(barco, direccion['dir'], direccion['cant']);
                postMove(token, gameId, barco, direccion['dir'], direccion['cant']);
                escribirInfo('Elige una acción');
              }
              resetearTablero();
              iniciarTurno();
            } else if (barco[0] === 'C') {
              if (direccion['cant'] <= 3) {
                cambiarBoteEnTablero(barcoPos, nuevaPos, barco);
                anotarMovidaUsuario(barco, direccion['dir'], direccion['cant']);
                postMove(token, gameId, barco, direccion['dir'], direccion['cant']);
                escribirInfo('Elige una acción');
              }
              resetearTablero();
              iniciarTurno();
            } else if (barco[0] === 'D') {
              if (direccion['cant'] <= 2) {
                cambiarBoteEnTablero(barcoPos, nuevaPos, barco);
                anotarMovidaUsuario(barco, direccion['dir'], direccion['cant']);
                postMove(token, gameId, barco, direccion['dir'], direccion['cant']);
                escribirInfo('Elige una acción');
              }
              resetearTablero();
              iniciarTurno();
            } else if (barco[0] === 'P') {
              if (direccion['cant'] <= 1) {
                cambiarBoteEnTablero(barcoPos, nuevaPos, barco);
                anotarMovidaUsuario(barco, direccion['dir'], direccion['cant']);
                postMove(token, gameId, barco, direccion['dir'], direccion['cant']);
                escribirInfo('Elige una acción');
              }
              resetearTablero();
              iniciarTurno();
            }
          }
        }
      });
    }
  });
}

function disparar(token, gameId) {
  $('[id$=-casilla]').click(function () {
    let barco = $(this).html();
    let casilla = this.id.split('-')[0] + this.id.split('-')[1];
    if (barco && barco !== 'RIP') {
      $(this).css("background-color", "green");
      escribirInfo(`Elige donde va a disparar ${barco}`);
      $('[id$=-casilla]').click(function () {
        let letra = this.id.split('-')[0];
        let numero = this.id.split('-')[1];
        let cordDisparo = posCord(letra + numero);
        let cordBarco = posCord(casilla);
        if (cordDisparo['x'] !== cordBarco['x'] && cordDisparo['y'] !== cordBarco['y']) {
          resetearTablero();
          iniciarTurno();
        } else {
          let distX = Math.abs(cordDisparo['x'] - cordBarco['x']);
          let distY = Math.abs(cordDisparo['y'] - cordBarco['y']);
          let maximo = Math.max(distX, distY);
          if (barco[0] === 'F') {
            if (maximo <= 2) {
              anotarDisparoUsuario(barco, cordDisparo);
              postShot(token, gameId, barco, cordDisparo);
              escribirInfo('Elige una acción');
            }
            resetearTablero();
            iniciarTurno();
          } else if (barco[0] === 'C') {
            if (maximo <= 2) {
              anotarDisparoUsuario(barco, cordDisparo);
              postShot(token, gameId, barco, cordDisparo);
              escribirInfo('Elige una acción');
            }
            resetearTablero();
            iniciarTurno();
          } else if (barco[0] === 'D') {
            if (maximo <= 3) {
              anotarDisparoUsuario(barco, cordDisparo);
              postShot(token, gameId, barco, cordDisparo);
              escribirInfo('Elige una acción');
            }
            resetearTablero();
            iniciarTurno();
          } else if (barco[0] === 'P') {
            if (maximo <= 5) {
              anotarDisparoUsuario(barco, cordDisparo);
              postShot(token, gameId, barco, cordDisparo);
              escribirInfo('Elige una acción');
            }
            resetearTablero();
            iniciarTurno();
          }
        }
      });
    }
  });
}

function escribirInfo(texto) {
  $('#info .texto-info').remove();
  let msg = $('<p></p>').addClass('texto-info').text(texto);
  $('#info').append(msg);
}

function iniciarTurno() {
  $('#cancelar').attr('disabled', true);
  $('#disparar').attr('disabled', false);
  $('#mover').attr('disabled', false);
}

function apretarAccion() {
  $('#cancelar').attr('disabled', false);
  $('#disparar').attr('disabled', true);
  $('#mover').attr('disabled', true);
}

function jugar() {
  $('#boton-jugar').hide();
  $('#botones-juego').show();
  $('#log').show();
  $('#info').show();
  iniciarTurno();
}

function botonJugar() {
  $('#gui .nave').remove();
  $('#gui .gui-texto').remove();
  $('#boton-jugar').show();
  $('#reiniciar').hide();
}

function clickTablero() {
  const texto1 = $('<p></p>').addClass('gui-texto').text('Haz click en una casilla para colocar una nave');
  const texto2 = $('<span></span>').addClass('gui-texto').text('Ahora colocando:');
  let idNaveActual = 'F1';
  let tipoNaveActual = 'Fragata';
  let ubicarBarcos = true;
  let nave = $('<span></span>').addClass('nave').text(`Fragata ${idNaveActual}`);
  let contador = 1;
  $('#gui').append(texto1, texto2, nave);

  // Ubicar los barcos
  $('[id$=-casilla]').click(function () {
    let letra = this.id.split('-')[0];
    let numero = this.id.split('-')[1];
    if (ubicarBarcos) {
      let casilla = $(this).text();
      // Checkear que casilla esté vacia
      if (casilla === '') {
        contador += 1;
        $(this).html(idNaveActual);
        if (idNaveActual == 'P1') {
          ubicarBarcos = false;
          botonJugar()
        }
      }

      // Escribir la siguiente nave en la gui
      $('#gui .nave').remove();
      if (contador <= 4) {
        idNaveActual = `F${contador}`;
        tipoNaveActual = 'Fragata';
      } else if (contador <= 7) {
        idNaveActual = `C${contador - 4}`;
        tipoNaveActual = 'Crucero';
      } else if (contador <= 9) {
        idNaveActual = `D${contador - 7}`;
        tipoNaveActual = 'Destructor';
      } else if (contador <= 10) {
        idNaveActual = `P${contador - 9}`;
        tipoNaveActual = 'Portaviones';
      }
      if (ubicarBarcos) {
        nave = $('<span></span>').addClass('nave').text(`${tipoNaveActual} ${idNaveActual}`);
        $('#gui').append(nave);
      }
    } else {

    }
  });
}

async function obtenerGameId(token) {
  let gameId = await fetch('https://battleship.iic2513.phobos.cl/games', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token
    },
  });

  gameId = await gameId.json();
  return gameId['gameId'];
}

async function obtenerFlotaEnemiga(token, gameId) {
  let flota = await fetch('https://battleship.iic2513.phobos.cl/games/' + gameId + '/test/ships', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token
    },
  });
  flota = await flota.json();
  return flota;
}

async function obtenerToken(email, studentNumber) {
  let token = await fetch('https://battleship.iic2513.phobos.cl/auth', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body: JSON.stringify({ email, studentNumber }),
  });
  token = await token.json();
  return token['token'];
}

$(async function () {
  $('#boton-jugar').hide();
  $('#botones-juego').hide();
  $('#log').hide();
  $('#info').hide();
  const email = 'movalle4@uc.cl';
  const numero = '17642299';
  const token = await obtenerToken(email, numero);
  console.log(token);
  const gameId = await obtenerGameId(token);
  console.log(gameId);
  const flotaEnemiga = await obtenerFlotaEnemiga(token, gameId);
  console.log(flotaEnemiga);


  clickTablero();
  $('#boton-jugar').click(function () {
    $('#reiniciar').hide();
    jugar();
  });
  // Reiniciar
  $('#reiniciar').click(function () {
    location.reload();
  });
  // Rendirse
  $('#rendirse').click(function () {
    // Resetear juego
    $('#botones-juego').hide();
    $('#info').hide();
    $('#log').hide();
    $('#reiniciar').show();
    $('#gui').prepend('<p>Perdiste</p>');
  })
  // Acciones
  $('#disparar').click(function () {
    apretarAccion();
    escribirInfo('Elige una nave');
    disparar(token, gameId);
  })
  $('#mover').click(function () {
    apretarAccion();
    escribirInfo('Elige una nave');
    moverBote(token, gameId);
  })
  $('#cancelar').click(function () {
    iniciarTurno();
    escribirInfo('Elige una acción');
  })
});